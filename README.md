## MSI Drivers package

### Usage
Inside cmd, use `cat.exe msi-drivers-a* > msi-drivers.exe` to create self extracting setup.

Once extracted, use PS or CMD to run `install.bat` to install required drivers.

Copyright © 2024 Manpreet Rai. All rights reserved.
